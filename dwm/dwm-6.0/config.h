/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

/* appearance */
static const char font[]      = "Overpass:Regular:size=9:antialias=true:hinting=true";
#define NUMCOLORS 9

/* Macro for unused actions to silence warnings */
#define UNUSED(action) { 0, 0, action, { .i = 0 } },

/* Tag bit-mask macros */
#define TAG(N) (1 << (N - 1))
#define MT2(A, B) (TAG(A) | TAG(B))
#define MT3(A, B, C) (TAG(A) | TAG(B) | TAG(C))
#define MT4(A, B, C, D) (TAG(A) | TAG(B) | TAG(C) | TAG(D))

/* Rule macros */
#define CLASS(C) (C), NULL, NULL
#define INSTANCE(I) NULL, (I), NULL
#define TITLE(T) NULL, NULL, (T)
#define CLASS_N_TITLE(C, T) (C), NULL, (T)

#define BLUE   "#00BFFF"
#define WHITE  "#cdcdcd"
#define BLACK  "#000000"
#define ORANGE "#ff8c00"
#define RED    "#ff0000"
#define GRAY   "#666666"
#define PURPLE "#BF5FFF"
#define LIGHT_GREEN "#76EE00"
#define LIGHT_BLUE "#7DC1CF"
#define AZURE "#80d9d8"
#define LIGHT_ORANGE "#FFA07A"
#define LIGHT_GREEN2 "#78AB46"
#define MAGENTA "#7E62B3"

static const char colors[NUMCOLORS][ColLast][9] = {
 /* border  foreground  background */
    {  BLACK, WHITE,   BLACK },  // 1 = normal
    {  RED,   BLUE,  BLACK },  // 2 = selected
    {  BLACK, RED,     BLACK },  // 3 = urgent
    {  BLACK, GRAY,    BLACK },  // 4 = occupied
    {  BLACK, ORANGE,  BLACK },
    {  BLACK, LIGHT_GREEN,   BLACK },
    {  BLACK, PURPLE,  BLACK },
    {  BLACK, LIGHT_BLUE,  BLACK },
    {  BLACK, MAGENTA,  BLACK },
};
static const unsigned int systrayspacing  = 2;  // Systray spacing
static const unsigned int borderpx        = 2;  // Border pixel of windows
static const unsigned int gappx           = 8;  // Gap pixel between windows
static const unsigned int snap            = 2;  // Snap pixel
static const Bool showbar                 = True;  // False means no bar
static const Bool showsystray             = True; // False means no systray
static const Bool topbar                  = True;  // False means bottom bar

/* layout(s) */
static const float mfact      = 0.70;     // factor of master area size [0.05..0.95]
static const int nmaster      = 1;        // number of clients in master area
static const Bool resizehints = False;    // True means respect size hints in tiled resizals

static const Layout layouts[] = {
  /* symbol   gaps    arrange */
  { " [T] ",  True,   tile    },
  { " [Bs] ", True,   bstack  },
  { " [M] ",  False,  monocle },
  { " [F] ",  False,  NULL    },
  { .symbol =   NULL,   .arrange = NULL    },
};

/* tagging */
static const Tag tags[] = {
  /* name       layout         mfact    nmaster */
  { "cmd",    &layouts[0],      -1,      -1 },
  { "web",    &layouts[2],      -1,      -1 },
  { "im",     &layouts[2],      -1,      -1 },
  { "mail",   &layouts[0],      -1,      -1 },
  { "dev",    &layouts[0],      -1,      -1 },
  { "chill",  &layouts[0],      -1,      -1 },
  { "vm",     &layouts[2],      -1,      -1 },
  { "doc",    &layouts[2],      -1,      -1 },
};

static const char hangouts_class[]  = "crx_knipolnnllmklapflnccelgolnpehhpl";

static const Rule rules[] = {
   /*class                         tags            isfloating       monitor */
  { CLASS("Firefox"),             TAG(2),            False,           -1 },
  { INSTANCE("chromium"),         TAG(2),            False,           -1 },
  { INSTANCE("google-chrome"),    TAG(2),            False,           -1 },
  { INSTANCE("vivaldi"),          TAG(2),            False,           -1 },
  { CLASS("Opera"),               TAG(2),            False,           -1 },
  { CLASS("Skype"),               TAG(3),            False,           -1 },
  { CLASS("Rambox"),              TAG(3),            False,           -1 },
  { TITLE("Messenger"),           TAG(3),            False,           -1 },
  { TITLE("yakyak"),              TAG(3),            False,           -1 },
  { CLASS("Viber"),               TAG(3),            False,           -1 },
  { CLASS("Thunderbird"),         TAG(4),            False,           -1 },
  { CLASS("Geary"),               TAG(4),            False,           -1 },
  { CLASS("Gmpc"),                TAG(6),            False,           -1 },
  { CLASS("Cantata"),             TAG(6),            False,           -1 },
  { CLASS("Thunar"),              TAG(6),            False,           -1 },
  { CLASS("Virtualbox"),          TAG(7),            False,           -1 },
  { CLASS("Pcmanfm"),             TAG(6),            False,           -1 },
  { CLASS("File-roller"),         TAG(6),            False,           -1 },
  { CLASS("Zathura"),             TAG(8),            False,           -1 },
  { CLASS("Evince"),              TAG(8),            False,           -1 },
  { CLASS("Atril"),               TAG(8),            False,           -1 },
  { CLASS("Spotify"),             TAG(6),            False,           -1 },
  { INSTANCE(hangouts_class),     TAG(3),            False,           -1 },
};

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
  { MODKEY,            KEY, view, {.ui = 1 << TAG} }, \
  { MODKEY|ShiftMask,  KEY, tag,  {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *interrobang[] = { "interrobang", NULL };
static const char *chromium[] = { "chromium", NULL };
static const char *firefox[] = { "firefox-beta", NULL };
static const char *opera[] = { "opera", NULL };
static const char *touchpadcmd[] = { "/bin/sh", "/home/ilusi0n/.scripts/touchpad_toggle", NULL };
static const char *termcmd[]  = { "urxvtc", NULL };
static const char *pcmanfm[]  = { "pcmanfm", NULL };
//static const char *thunar[]  = { "thunar", NULL };
static const char *thunderbird[]  = { "thunderbird", NULL };
static const char *rambox[]  = { "rambox", NULL };
static const char *gmpc[]  = { "gmpc", NULL, "Gmpc" };
static const char *lock[]  = { "/bin/sh", "/home/ilusi0n/.scripts/lock", NULL };
static const char *upvol[] = { "/bin/sh", "/home/ilusi0n/.scripts/sound", "up", NULL };
static const char *downvol[] = { "/bin/sh", "/home/ilusi0n/.scripts/sound", "down", NULL };
static const char *mpdprev[] = {"mpc", "prev", "-q", NULL };
static const char *mpdtoggle[] = {"mpc", "toggle", "-q", NULL };
static const char *mpdnext[] = {"mpc", "next", "-q", NULL };
static const char *mpdstop[] = {"mpc", "stop", "-q", NULL };
static const char *upbri[] = { "light", "-A", "10", NULL };
static const char *downbri[] = { "light", "-U", "10", NULL };
static const char *print[] = { "/bin/sh", "/home/ilusi0n/.scripts/print",NULL};

static Key keys[] = {
  /* modifier                     key        function        argument */
  { ALTKEY,                       XK_o,      spawn,          {.v = opera } },
  { ALTKEY,                       XK_c,      spawn,          {.v = chromium } },
  { ALTKEY,                       XK_f,      spawn,          {.v = firefox } },
  { ALTKEY,                       XK_m,      spawn,          {.v = gmpc } },
  { ALTKEY,                       XK_2,      spawn,          {.v = pcmanfm } },
  { ALTKEY,                       XK_z,      spawn,          {.v = touchpadcmd } },
  { ALTKEY,                       XK_s,      spawn,          {.v = rambox } },
  { ALTKEY,                       XK_t,      spawn,          {.v = thunderbird } },
  { ControlMask|ALTKEY,           XK_l,      spawn,          {.v = lock } },
  { 0,              XF86XK_AudioRaiseVolume, spawn,          {.v = upvol} },
  { 0,              XF86XK_AudioLowerVolume, spawn,          {.v = downvol} },
  { 0,              XF86XK_AudioPrev,        spawn,          {.v = mpdprev } },
  { 0,              XF86XK_AudioPlay,        spawn,          {.v = mpdtoggle } },
  { 0,              XF86XK_AudioNext,        spawn,          {.v = mpdnext } },
  { 0,              XF86XK_AudioStop,        spawn,          {.v = mpdstop } },
  { MODKEY,                       XK_r,      spawn,          {.v = interrobang } },
  { MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
  { 0,                            XK_Print,  spawn,          {.v = print } },
  { 0,              XF86XK_MonBrightnessDown,spawn,          {.v = downbri } },
  { 0,              XF86XK_MonBrightnessUp,  spawn,          {.v = upbri } },
  { MODKEY,                       XK_space,                 nextlayout,     {0} },
  { MODKEY,                       XK_Left,                  shiftview,      {.i = -1 } },
  { MODKEY,                       XK_Right,                 shiftview,      {.i = +1 } },
  { MODKEY|ControlMask,           XK_b,                     togglebar,      {0} },
  { MODKEY|ShiftMask,             XK_q,                     quit,           {0} },
  { MODKEY,                       XK_j,                     focusstack,     {.i = +1 } },
  { MODKEY,                       XK_k,                     focusstack,     {.i = -1 } },
  { MODKEY|ShiftMask,             XK_space,                 killclient,     {0} },
  { MODKEY,                       XK_h,                     setmfact,       {.f = -0.05} },
  { MODKEY,                       XK_l,                     setmfact,       {.f = +0.05} },
  { MODKEY,                       XK_equal,                 incnmaster,     {.i = +1 } },
  { MODKEY,                       XK_minus,                 incnmaster,     {.i = -1 } },
  { MODKEY|ShiftMask,             XK_f,                     togglefloating, {0} },
  { MODKEY,                       XK_t,                     setlayout,      {.v = &layouts[0] } },
  { MODKEY,                       XK_b,                     setlayout,      {.v = &layouts[1] } },
  { MODKEY,                       XK_m,                     setlayout,      {.v = &layouts[2] } },
  { MODKEY,                       XK_f,                     setlayout,      {.v = &layouts[3] } },
  UNUSED(prevlayout)
  UNUSED(zoom)
  UNUSED(tagmon)
  UNUSED(focusmon)

  TAGKEYS(              XK_1,                    0)
  TAGKEYS(              XK_2,                    1)
  TAGKEYS(              XK_3,                    2)
  TAGKEYS(              XK_4,                    3)
  TAGKEYS(              XK_5,                    4)
  TAGKEYS(              XK_6,                    5)
  TAGKEYS(              XK_7,                    6)
  TAGKEYS(              XK_8,                    7)
};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkStatusText,        0,              Button4,        spawn,          {.v = upvol } },
    { ClkStatusText,        0,              Button1,        focusstack,     {.i = -1} },
    { ClkStatusText,        0,              Button3,        nextlayout,     {0} },
    { ClkStatusText,        0,              Button5,        spawn,          {.v = downvol } },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkClientWin,         MODKEY,         Button1,        tilemovemouse,  {0} },
};
