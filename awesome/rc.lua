-- {{{ Required libraries
gears     = require("gears")
awful     = require("awful")
awful.rules     = require("awful.rules")
require("awful.autofocus")
wibox     = require("wibox")
beautiful = require("beautiful")
naughty   = require("naughty")
drop      = require("scratchdrop")
lain      = require("lain")
vicious   = require("vicious")
-- }}}

-- {{{ Error handling
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
    title = "Oops, there were errors during startup!",
    text = awesome.startup_errors })
end

do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
        title = "Oops, an error happened!",
        text = err })
        in_error = false
    end)
end
-- }}}

-- }}}

-- {{{ Variable definitions

-- beautiful init
beautiful.init(os.getenv("HOME") .. "/.config/awesome/themes/multicolor/theme.lua")

-- common
modkey     = "Mod4"
altkey     = "Mod1"
terminal   = "urxvtc"
editor     = os.getenv("EDITOR") or "nano" or "vi"
editor_cmd = terminal .. " -e " .. editor

-- user defined
exec       = awful.util.spawn
print      = "/home/ilusi0n/.scripts/print"
lock       = "/home/ilusi0n/.scripts/lock"
upvol      = "/home/ilusi0n/.scripts/sound up"
downvol    = "/home/ilusi0n/.scripts/sound down"
touchpad   = "/home/ilusi0n/.scripts/touchpad_toggle"

layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.max,
}
-- }}}

-- {{{ Tags
tags = {
    names = { "cmd", "web", "im", "mail", "dev", "chill", "vm", "doc" },
    layout = { layouts[1], layouts[2], layouts[2], layouts[1], layouts[1], layouts[1],layouts[2],layouts[2] }
}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag(tags.names, s, tags.layout)
end
-- }}}

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}

-- Includes
local includes = {"functions", "keys", "rules", "signals"}
for i, module in ipairs(includes) do
    --print("*** Loading " .. module)
    require(module)
end

-- Automatically started apps
local apps = {"pcmanfm -d", "/home/ilusi0n/.scripts/touchpad_toggle",
"ibus-daemon -s -x -d"}
for i, app in ipairs(apps) do
    --print("*** Starting " .. app)
    run_once(app)
end
