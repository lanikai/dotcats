-- {{{ Mouse Bindings
root.buttons(awful.util.table.join(
--awful.button({ }, 4, awful.tag.viewnext),
--awful.button({ }, 5, awful.tag.viewprev)

))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(

-- Tag browsing
awful.key({ modkey }, "Left",   awful.tag.viewprev       ),
awful.key({ modkey }, "Right",  awful.tag.viewnext       ),

-- client focus
awful.key({ modkey,           }, "j",
function ()
    awful.client.focus.byidx( 1)
    if client.focus then client.focus:raise() end
end),
awful.key({ modkey,           }, "k",
function ()
    awful.client.focus.byidx(-1)
    if client.focus then client.focus:raise() end
end),

-- Show/Hide Wibox
awful.key({ modkey }, "b", function ()
    --mywibox[mouse.screen].visible = not mywibox[mouse.screen].visible
    mybottomwibox[mouse.screen].visible = not mybottomwibox[mouse.screen].visible
end),

-- Layout manipulation
awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
awful.key({ altkey,           }, "Tab",
function ()
    awful.client.focus.byidx(-1)
    if client.focus then
        client.focus:raise()
    end
end),
awful.key({ modkey,           }, "l",      function () awful.tag.incmwfact( 0.05)    end),
awful.key({ modkey,           }, "h",      function () awful.tag.incmwfact(-0.05)    end),
awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),

-- Standard program
awful.key({ modkey,           }, "Return", function () exec(terminal) end),
awful.key({ modkey, "Control" }, "r",      awesome.restart),
awful.key({ modkey, "Shift"   }, "q",      awesome.quit),

-- Dropdown terminal
awful.key({ modkey,           }, "z",      function () drop(terminal) end),

-- ALSA volume control
awful.key({ }, "XF86AudioRaiseVolume",
function ()
    os.execute(string.format(upvol, volumewidget.channel))
    volumewidget.update()
end),
awful.key({}, "XF86AudioLowerVolume",
function ()
    os.execute(string.format(downvol, volumewidget.channel))
    volumewidget.update()
end),

-- MPD control
awful.key({}, "XF86AudioStop",
function ()
    awful.util.spawn_with_shell("mpc stop -q")
    volumewidget.update()
end),
awful.key({}, "XF86AudioNext",
function ()
    awful.util.spawn_with_shell("mpc next -q")
    volumewidget.update()
end),
awful.key({ }, "XF86AudioPrev",
function ()
    awful.util.spawn_with_shell("mpc prev -q")
    volumewidget.update()
end),
awful.key({ }, "XF86AudioPlay",
function ()
    awful.util.spawn_with_shell("mpc toggle -q")
    volumewidget.update()
end),

-- Copy to clipboard
--awful.key({ modkey }, "c", function () os.execute("xsel -p -o | xsel -i -b") end),

-- User programs
awful.key({ altkey, }, "c", function() run_or_raise("chromium", { name = "Chromium" }) end),
awful.key({ altkey, }, "f", function() run_or_raise("firefox", { name = "Firefox" }) end),
awful.key({ altkey, }, "o", function() run_or_raise("opera", { name = "Opera" }) end),
awful.key({ altkey, }, "2", function() exec("pcmanfm") end),
awful.key({ altkey, }, "s", function() exec("franz") end),
awful.key({ altkey, }, "t", function() exec("thunderbird") end),
awful.key({ modkey, }, "r", function() exec("interrobang") end),
awful.key({ altkey, }, "m", function() exec("gmpc") end),
awful.key({ altkey, "Control" }, "l", function() exec(lock) end),
awful.key({ }, "Print", function () exec(print) end),
awful.key({ "Control" }, "Right", function() exec("light -A 10") end),
awful.key({ "Control" }, "Left", function () exec("light -U 10") end),
awful.key({ altkey },"z",function () exec(touchpad) end)
)

clientkeys = awful.util.table.join(
awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
awful.key({ modkey, "Shift"   }, "space",  function (c) c:kill()                         end),
awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle)
)

-- Bind all key numbers to tags.
-- be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
    -- View tag only.
    awful.key({ modkey }, "#" .. i + 9,
    function ()
        local screen = mouse.screen
        local tag = awful.tag.gettags(screen)[i]
        if tag then
            awful.tag.viewonly(tag)
        end
    end),
    -- Toggle tag.
    awful.key({ modkey, "Control" }, "#" .. i + 9,
    function ()
        local screen = mouse.screen
        local tag = awful.tag.gettags(screen)[i]
        if tag then
            awful.tag.viewtoggle(tag)
        end
    end),
    -- Move client to tag.
    awful.key({ modkey, "Shift" }, "#" .. i + 9,
    function ()
        if client.focus then
            local tag = awful.tag.gettags(client.focus.screen)[i]
            if tag then
                awful.client.movetotag(tag)
            end
        end
    end),
    -- Toggle tag.
    awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
    function ()
        if client.focus then
            local tag = awful.tag.gettags(client.focus.screen)[i]
            if tag then
                awful.client.toggletag(tag)
            end
        end
    end))
end

clientbuttons = awful.util.table.join(
awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
awful.button({ modkey }, 1, awful.mouse.client.move),
awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}
